### eslint

    可组装的JavaScript和JSX检查工具

1. "off" or 0 - 关闭规则
2. "warn" or 1 - 将规则视为一个警告（不会影响退出码）
3. "error" or 2 - 将规则视为一个错误 (退出码为 1)

- 新建一个.eslintrc 的文件
- 把 package.json 里面的配置粘贴出来，放进去
- 添加规则 rules(见官网)
- extends 继承语法包
- 添加 eslint 的忽略文件

### babel

    Babel 是一个 JavaScript 编译器

jsx 就是一个语法糖，React.createElement();
虚拟 dom:描述 dom 节点的 js 对象

- 新建一个 babel.config.js 的配置文件
- 把 package.json 里面的配置粘贴出来，放进去
- 下载 transform-remove-console
- 在配置文件里面判断，如果是生产环境的话，删除 console

### prettire（和 eslint 一起用的时候，会有冲突）

    美化代码格式的

- 去 prettire 官网，copy prettire 的配置文件
- 新建一个.prettierrc 文件，把刚刚粘贴的配置放进来
- 下载 prettier
- 下载 eslint-config-prettier 禁用与 Prettier 冲突的规则
- 下载 eslint-plugin-prettier 使用 Prettier 的规则
- 在 eslintrc 的配置文件里面启用 prettier
- 在 eslintrc 里面添加 prettier 的规则

### lint-staged

    lint-staged能够让lint只检测暂存区的文件

- 安装 husky
- 安装 lint-staged
- 新建.lintstagedrc 的配置文件
- 在 package.json 的 script 脚本里面添加"prepare": "husky install"
- 执行 npm run prepare

- 执行 npx husky add .husky/pre-commit "npx lint-staged --allow-empty $1"（会在 husky 文件夹里面看到一个 pre-commit 的文件）
- 测试（git add . git commit -m "描述"）

### commitlint （检测提交的记录是否符合规范）

- 下载 npm install -D @commitlint/config-conventional @commitlint/cli
- 新建 commitlint.config.js，把配置文件放进去
- 执行 npx husky add .husky/commit-msg "npx commitlint --edit $1"
- 测试（git add . git commit -m "feat: 描述"）

### husky

### 接口文档

1. 接口名称 /api/list
2. 接口的请求方式 post get
3. 接口的参数 （参数个数、参数描述、参数的类型、参数是否是必传的）
4. 接口的返回值 （返回值的描述）

### useEffect

1. 有 2 个参数（()=>{},[依赖]）
2. 对比类组件来说

- componentDidMount ===> useEffect(()=>{},[])
- componentDidUpdate ===> useEffect(()=>{},[依赖值])
- componentWillUnmount===> useEffect(()=>{return ()=>{销毁定时器，全局变量等}},[])

### 函数四要素

1. 函数的参数
2. 函数的返回值
3. 函数的作用
4. 函数在哪调用

### 主题切换 (https://juejin.cn/post/7003315163625422879)

1. 下载 less less-loader
2. 在 webpack.config.js 里面添加 less 的配置
3. 在 styles 下新建 theme.less(全局样式的变量)，新建 variable.less(具体某个元素引用的变量)
4. 在 js 里面实现切换
5. 配置 less 全局变量
6. 在 webpack.config.js 里面配置全局变量（https://blog.csdn.net/jason_renyu/article/details/107059919）

### 配置别名

在 webpack.config.js 里面找 alias 这个字段

### import 指令和 import 方法的区别

1.  指令是编译时加载
2.  方法运行时加载

### 懒加载原理

### export 和 export default 区别

### js 数据类型

### 实现深拷贝的方式

1. JSON.parse(JSON.string) 为什么能实现深拷贝？ 有什么缺点？（哪些拷贝不了）
2. 递归实现

### 数组、对象常用的方法

### cssModule css in js

### commonJs 规范，只能用在服务端

1. require() 引入
2. module.exports exports 抛出

### esModule （客户端）

1. import 引入
2. export 抛出

### 错误边界

1. 错误边界是什么？

   是一个 React 组件，它可以 在子组件树的任何位置捕获 JavaScript 错误，记录这些错误，并显示一个备用 UI ，而不是使整个组件树崩溃。

2. 在哪捕捉错误？

   错误边界(Error Boundaries) 在渲染，生命周期方法以及整个组件树下的构造函数中捕获错误。

3. 哪些地方不能捕捉错误？

- 事件处理 （了解更多）
- 异步代码 （例如 setTimeout 或 requestAnimationFrame 回调函数）
- 服务端渲染
- 错误边界自身抛出来的错误 （而不是其子组件）

4. 仅有类组件可以成为错误边界。
5. 错误边界(Error Boundaries) 仅可以捕获其子组件的错误

### 高阶函数 是至少满足下列一个条件的函数

1. 接受一个或多个函数作为输入（参数）
2. 输出（返回）一个函数

数组： map forEach filter every some

### 国际化实现

1. 下载 npm install react-i18next i18next --save
2. 在 src 下边新建 i18n 文件夹，在文件夹里面新建 en.json（英文） zh.json(中文) config.js（i18n 初始的配置）
3. 在 index.js 引入配置文件
4. 在组件里面使用

- const { t } = useTranslation(); t 函数来翻译
- <h3>{t('header.home')}</h3>
- const changeLange = () => {
  i18n.changeLanguage('en'); // val 入参值为'en'或'zh'
  };

### mobx

1. npm install --save mobx mobx-react-lite（函数组件） mobx-react（函数组件、类型组件）

### 伪数组转真正的数组

1. [...children] es6
2. Array.from() es6
3. Array.prototype.slice.apply(ali) es5
