async function async1() {
  console.log('async1 start1');
  await async2();
  console.log('async1 end 2');
}
async function async2() {
  new Promise(function (resolve) {
    console.log('promise1 3');
    resolve();
  }).then(function () {
    console.log('promise2 4');
  });
}
console.log('script start 5');
setTimeout(function () {
  console.log('setTimeout 6');
}, 0);
async1();
new Promise(function (resolve) {
  console.log('promise3 7');
  resolve();
}).then(function () {
  console.log('promise4 8');
});
console.log('script end 9');
