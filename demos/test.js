console.log('开始1'); // 1
// pending--->resolve pending--->reject  状态是不可逆的
new Promise((resolve, reject) => {
  console.log('2'); // 2
  resolve();
}).then(() => {
  //异步 微任务
  console.log(4);
  setTimeout(() => {
    console.log(8);
  }, 0);
});

setTimeout(() => {
  // 异步  宏任务
  console.log(5);
  new Promise((resolve, reject) => {
    console.log(6);
    resolve();
  }).then(() => {
    console.log(7);
  });
}, 0);

//先执行微任务再执行宏任务
console.log(3); // 3

// js：eventloop（事件循环）
// js单线程，从上往下执行
// 先执行同步脚本，执行过程中遇到异步脚本，把异步脚本放到异步队列里面
// 异步队列分微队列和宏队列（微任务都放在微队列里面，宏任务都放在宏队列里面）
// 同步脚本都执行完之后，再执行异步。执行异步的时候，先执行微任务再执行宏任务
// 当一个宏任务执行完之后，会去微队列里面看下有没有新产生的微任务。
// 如果有，会先执行微任务，再执行宏任务
// 只有当微队列清空的时候，才会执行下一个宏任务

// Promise.then()  Object.observe  MutationObserver  process.nextTick(node)   “async/await”

// 宏任务：<script>整体代码、setTimeout、setInterval、setImmediate、Ajax、DOM事件
