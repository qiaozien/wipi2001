import logo from './logo.svg';
import './App.css';
import './styles/common.less';
import './styles/theme.less';
import './styles/variable.less';
import { useState, useEffect } from 'react';
function App() {
  const [flag, setFlag] = useState(false);

  const changeTheme = () => {
    console.log('改变主题');
    setFlag(!flag);
  };
  useEffect(() => {
    const root = document.documentElement;
    root.style.setProperty('--background-color', flag ? 'green' : '#fff');
    root.style.setProperty('--color', flag ? '#fff' : '#000');
  }, [flag]);
  return (
    <div className="App">
      <header>
        <button onClick={changeTheme}>切换主题</button>
      </header>
      <section>
        <h3>内容</h3>
      </section>
    </div>
  );
}

export default App;
