import './App.css';
import './styles/common.less';
import RouterView from '@/router/routerview';
import ErrorBoundary from './component/ErrorBoundary/ErrorBoundary';
import Provider from './store/provide';
import store from './store/index';
function App() {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <RouterView />
      </Provider>
    </ErrorBoundary>
  );
}

export default App;
