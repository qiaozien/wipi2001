import { useState, useEffect } from 'react';
// 获取img对象
const getImage = (imgurl) => {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.crossOrigin = 'Anonymous'; //前端需要做的，要配合后端一起。后端需要设置响应头access-control-allow-origin: *
    img.src = imgurl;
    img.onload = function () {
      resolve(img);
    };
    img.onerror = function (error) {
      reject(error);
    };
  });
};
const drawCanvas = async (ctx, data, canvasDom) => {
  //绘制填充背景
  ctx.fillStyle = '#fff';
  ctx.fillRect(0, 0, 400, 668);
  //绘制标题

  ctx.font = '20px Georgia';
  ctx.fillStyle = '#000';
  ctx.fillText(data.title, 124, 30);

  //绘制标题底部的线

  ctx.beginPath();
  ctx.strokeStyle = 'green'; // 红色路径
  ctx.moveTo(0, 45);
  ctx.lineTo(400, 45);
  ctx.stroke(); // 进行绘制

  // // 绘制图片的时候没有问题，在导出画布的时候，就会失败。因为外部链接这种，会污染画布。
  let img = await getImage(data.cover);
  ctx.drawImage(img, 50, 130, 300, 300);

  // 绘制图片下边的文字
  ctx.font = '12px Georgia';
  ctx.fillStyle = '#000';
  ctx.fillText(data.title, 50, 450);
  ctx.fillText(data.summary, 50, 470);

  //绘制二维码
  const qrcode = document.getElementById('qrcode');
  const qrcodeimg = await getImage(qrcode.toDataURL());
  ctx.drawImage(qrcodeimg, 50, 490, 120, 120);

  // 绘制二维码后边的文字
  ctx.fillStyle = '#000';
  ctx.fillText(data.title, 190, 500);

  // 生产url 把Canvas转成图片
  const imgUrl = canvasDom.current.toDataURL();
  // console.log(imgUrl, 'imgUrl');
  return imgUrl;

  // console.log(url, 'qrcode');
};
const getInitCanvas = async (data, canvasDom) => {
  canvasDom.current.width = 400;
  canvasDom.current.height = 668;
  const ctx = canvasDom.current.getContext('2d'); //上下文对象
  //绘制
  let imgurl = await drawCanvas(ctx, data, canvasDom);
  //   setUrl(imgurl);
  // console.log(imgurl, '12313121------------');
  return imgurl;
};
const useCanvas = (data, canvasDom) => {
  const [url, setUrl] = useState('');
  useEffect(() => {
    (async () => {
      let url = await getInitCanvas(data, canvasDom);
      setUrl(url);
    })();
  }, [data]);

  return url;
};

export default useCanvas;
