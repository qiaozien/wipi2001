import { useState, useEffect } from 'react';
// const { list, error } = useRequest(api.getRecommend);
const useRequest = (service, options) => {
  //service===>api.getRecommend
  console.log(service, 'service');
  const [list, setList] = useState([]);
  const [error, setError] = useState({});
  const getList = async () => {
    //发接口请求
    let res = await service(options?.data);
    if (res.data?.statusCode == 200) {
      //请求成功
      setList(res.data?.data || []);
    } else {
      //失败
      setError(res.data?.error);
    }
  };
  const run = () => {
    //定义了一个run函数，当run函数调用的时候，才会发起接口请求
    getList();
  };
  useEffect(() => {
    //判断是否是默认请求
    if (!options?.manual) {
      //默认
      getList();
    }
  }, []);
  return {
    list,
    error,
    run,
  };
};

// let {data,error,run} = useRequest(()=>{})

export default useRequest;
