import { makeAutoObservable } from 'mobx';
import i18n from 'i18next';
import api from '@/api/index';
class TitleStore {
  val = 'zh';
  flag = false;
  list = [];
  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }
  changeFlag() {
    this.flag = !this.flag;
    const root = document.documentElement;
    root.className = !this.flag ? 'light' : 'dark';
  }
  changeLang() {
    console.log('changelang');
    this.val = this.val === 'en' ? 'zh' : 'en';
    i18n.changeLanguage(this.val); // val入参值为'en'或'zh'
  }
  async getlist() {
    let res = await api.getRecommend();

    this.list = res.data.data;
  }
}

export default new TitleStore();
