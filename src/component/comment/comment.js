import React from 'react';
import CommentModule from './comment.module.less';
import * as dayjs from 'dayjs';
import * as RelativeTime from 'dayjs/plugin/relativeTime'; // 导入插件
import 'dayjs/locale/zh-cn'; // 导入本地化语言

dayjs.extend(RelativeTime); // 使用插件
dayjs.locale('zh-cn'); // 使用本地化语言
const Comment = ({ list }) => {
  return (
    <>
      {list &&
        list.map((item) => (
          <div className={CommentModule.comment} key={item.id}>
            <div className={CommentModule.header}>
              <span>头像</span>
              <div>
                <span>{item.name}</span>
                {item.replyUserName && <div>回复{item.replyUserName}</div>}
              </div>
            </div>
            <div className={CommentModule.content}>{item.content}</div>
            <div className={CommentModule.footer}>
              <span>{item.userAgent}</span>
              <span>{dayjs(new Date(item.updateAt)).fromNow()}</span>
              <span>回复</span>
            </div>
            {item.children && <Comment list={item.children} />}
          </div>
        ))}
    </>
  );
};

export default Comment;
