import React from 'react';

import { useState, useEffect } from 'react';
import { indexRoutes } from '@/router/router.config.js';
import { NavLink } from 'react-router-dom';
import headerModule from './header.module.less';
import { useTranslation } from 'react-i18next';
import { useRootStore } from '@/store/provide';
import { observer } from 'mobx-react-lite';
const Header = () => {
  const {
    store: {
      titleStore: { val, flag, changeLang, changeFlag },
    },
  } = useRootStore();
  console.log(val, 'store&&&&&');
  const { t } = useTranslation();

  useEffect(() => {
    // getlist();
  }, []);

  // useEffect(() => {
  //   //设置初始默认值
  //   const root = document.documentElement;
  //   root.className = !flag ? 'light' : 'dark';
  // }, [flag]);
  return (
    <div className={headerModule.header}>
      <ul>
        {indexRoutes &&
          indexRoutes.map((item) => (
            <li key={item.path}>
              <NavLink to={item.path}>{t('header.' + item.meta.title)}</NavLink>
            </li>
          ))}
      </ul>
      <button onClick={changeFlag}>切换主题</button>
      <button onClick={changeLang}>国际化</button>
    </div>
  );
};

export default observer(Header);
