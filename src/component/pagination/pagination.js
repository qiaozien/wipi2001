import React, { useState } from 'react';
import PaginationModule from './pagination.module.less';
const Pagination = ({ total, page = 1, pageSize = 5, onChange }) => {
  const count = Math.ceil(total / pageSize);
  const [ind, setInd] = useState(0);
  // console.log(count, new Array(2), 'new Array(count).fill(count)');
  const changePage = (ind) => {
    onChange(ind);
  };
  return (
    <div className={PaginationModule.pagination}>
      <div>
        {count &&
          new Array(count).fill(count).map((item, index) => (
            <span
              onClick={() => changePage(index + 1)}
              className={page == index + 1 ? PaginationModule.active : ''}
            >
              {index + 1}
            </span>
          ))}
      </div>
    </div>
  );
};

export default Pagination;
