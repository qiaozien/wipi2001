import React from 'react';
import Header from '@/component/header/header';
import { Outlet } from 'react-router-dom';
import indexModule from './index.module.less';
const Index = () => {
  return (
    <div className={indexModule.index}>
      <Header />
      <Outlet />
    </div>
  );
};

export default Index;
