import React, { useState, useEffect } from 'react';
import api from '@/api';
import useRequest from '@/hooks/useRequest';
const Article = () => {
  // {data,error}=useRequest(fn,{})
  //默认发请求

  const { list, error, run } = useRequest(api.getRecommend, {
    data: { page: 1 },
    manual: true, //不走默认请求，需要手动触发请求
  });
  console.log(list, 'ddd', error);
  const handleClick = () => {
    //点击的时候发请求
    run();
  };
  return (
    <div>
      <div>
        {list && list.map((item) => <h3 key={item.id}>{item.title}</h3>)}
      </div>
      <button onClick={handleClick}>点击</button>
    </div>
  );
};

export default Article;
